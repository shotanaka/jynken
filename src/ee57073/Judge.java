/*
 * Judge.java
 */
package ee57073;

/**
 * じゃんけんの結果を表すインタフェースです．
 * @author 田中 翔
 * @version 1.0.0
 */
public interface Judge{

    /**
	 * じゃんけんの結果(x = win,lose,draw)を取得します．
     * @return x
     */
     String getVisualJudge();
	
	 /**
     * じゃんけんの出す手を見ます。
	 * @param playerHand プレイヤーの手
	 * @param enemyHand 相手の出す手
     */
	void setJudge(int playerHand,int enemyHand);
    
}