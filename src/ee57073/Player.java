/*
 * Player.java
 */
package ee57073;

/**
 * JankenHandインタフェースの可変の実装クラスです．
 * @author 田中 翔
 * @version 1.0.0
 * @see JankenHand
 */
public class Player implements JankenHand {

    /**
     *フィールド
     */
    private int playerHand;

    /**
     * コンストラクタ
     */
    public Player() {
        
    }
	
	/**
	 * String型のgetter
	 */
	@Override
	public final String getVisualHand(){
		switch (this.playerHand) {
                case 0:
                   return "rock"; // グー
                case 1:
                   return "scissors"; // チョキ
                case 2:
                    return "paper"; // パー
                default:
                    throw new AssertionError("internal error");
            }
	}
	

    /**
     * getterの追加
	 * @return this.playerHand
     */
    @Override
    public final int getHand(){
		return this.playerHand;
    }
	

    /**
     * setterの追加
     */
    @Override
    public final void setHand(int playerHand){
        this.playerHand = playerHand;
    }


}