/*
 * Referee.java
 */
package ee57073; 

/**
 * JankenHandインタフェースの可変の実装クラスです．
 * @author 田中 翔
 * @version 1.0.0
 * @see Judge
 */
public class Referee implements Judge {

    /**
     *フィールド
     */
    private int playerHand,enemyHand;


    /**
     * コンストラクタ
     */
    public Referee() {
        
    }


    /**
     * getterの追加
     */
    @Override
    public final String getVisualJudge(){
    	switch ((this.playerHand - this.enemyHand + 3) % 3) {
               case 0:
                     return "draw"; // 引き分け
                 case 1:
                     return "lose"; // 負け
                 case 2:
                     return "win"; // 勝ち
                 default:
                   throw new AssertionError("internal error");
             }
    }
	
    /**
     * setterの追加
	 * @param playerHand playerの手
	 * @param enemyHand enemyの手
     */
    @Override
    public void setJudge(int playerHand ,int enemyHand){
        this.playerHand = playerHand;
    	this.enemyHand = enemyHand;
    }


}