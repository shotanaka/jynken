/*
 * JankenHand.java
 */
package ee57073;

/**
 * じゃんけんで出す手をを表すインタフェースです．
 * @author 田中 翔
 * @version 1.0.0
 */
public interface JankenHand{

	/**
	 * x(=グー,チョキ,パー)の値を取得します。
	 * @return x;
	 */
	String getVisualHand();

    /**
	 * x(=0,1,2)を取得します．
     * @return x
     */
    int getHand();

    /**
     * handの値を格納します。
	 * @param hand 0,1,2の値
     */
    void setHand(int hand);

    
}