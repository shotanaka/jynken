/*
 * Enemy.java
 */
package ee57073;

/**
 * JankenHandインタフェースの可変の実装クラスです．
 * @author 田中 翔
 * @version 1.0.0
 * @see JankenHand
 */
public class Enemy implements JankenHand {

    /**
     *フィールド
     */
    private int enemyHand;

    /**
     * コンストラクタ
     */
    public Enemy() {
        
    }
	
	/**
	 * String型のgetter
	 */
	@Override
	public final String getVisualHand(){
		switch (this.enemyHand) {
                case 0:
                    return "rock"; // グー
                case 1:
                    return "scissors"; // チョキ
                case 2:
                    return "paper"; // パー
                default:
                    throw new AssertionError("internal error");
            }
	}

    /**
     * getterの追加
     */
    @Override
    public final int getHand(){
          return this.enemyHand;
    }

    /**
     * setterの追加
     */
    @Override
    public void setHand(int enemyHand){
        this.enemyHand = enemyHand;
    }


}