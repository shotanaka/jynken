/*
 * Main.java
 * ！！！！注意！！！！
 * これまでの授業で学習したオブジェクト指向の知識を総動員して，このソースコードを出来る限り改良してください．
 * クラスを新しく作る場合には，eeXXXXX（ee + 自分の学籍番号の下5桁）パッケージの直下に配置してください．
 * このプログラムに対する入力として，付属のinput.txtが与えられるものとします．
 * このプログラムに対して付属のinput.txtを入力した結果が，付属のoutput.txtです．
 * このプログラムを改良する際には，改良後の出力が改良前の出力とまったく同じになるようにしてください．
 * すなわち，改良後のプログラムに付属のinput.txtを入力として与えた場合に，
 * 付属のoutput.txtと一字一句同じ結果が得られなければなりません．
 * 念のための注意ですが，じゃんけんプログラム課題としての意図に反するような変更をしてはいけません．
 * たとえば，一切の判定ロジックを取り払って，System.out.println()を使って付属のoutput.txtの内容を
 * そのまま出力するようなプログラムに変更した場合は，課題の意図に反しているので未提出として扱います．
 */
package main;
import java.util.Random;
import java.util.Scanner;
import ee57073.*;

/**
 * ぼくのかんがえたさいきょうのじゃんけんげーむ．
 * 【ルール】
 * 日本の一般的なじゃんけんのルールに準ずる．
 * 【手の説明】
 * グー：rock
 * チョキ：scissors
 * パー：paper
 * 【手と数値との関係】
 * 0: rock
 * 1: scissors
 * 2: paper
 * 【説明】
 * （1）Scanner
 * Scannerは，キーボードから値を読み込むために使います．
 * このソース中では，プレイヤーのじゃんけんの手を読み込むために使っています．
 * （2）Random
 * Randomは，乱数生成器です．
 * このソース中では，CPU（相手）のじゃんけんの手を生成するために使っています．
 * 【使用方法】
 * java main.Main  input.txt  result.txt
 * ※javaのオプション指定は省略しています．
 * ここで，小なり（）はファイルからの入力を表すリダイレクト記号で，大なり（）はファイルへの出力を表すリダイレクト記号です．
 * result.txtがあなたのプログラムの出力結果です．
 * これをoutput.txtと比較して，相違がないことを確認してください．
 */
public final class Main {

    /**
     * mainメソッドです．課題のためにあえてひどく書いてあります．
     * @param args コマンドライン引数（使わない）
     */
    public static void main(String[] args) {

    Scanner scanner = new Scanner(System.in);// プレイヤーのじゃんけんの手の入力に使う
    ee57073.JankenHand Enemy1 = new Enemy(); //インターフェースを利用したポリモーフィズム
    ee57073.JankenHand Player1 = new Player(); //インターフェースを利用したポリモーフィズム
    ee57073.Judge Referee1 = new Referee();
    Random random = new Random(0L); // 乱数生成器（絶対に引数の値をいじらないこと）
    	
        while (scanner.hasNext()) { // 入力がある限り

            if (!scanner.hasNextInt()) { // 次の入力がない場合
                System.err.println("input error"); // エラー表示
                System.exit(1); // プログラムを異常終了する
            }

            Enemy1.setHand(random.nextInt(3)); // CPUのじゃんけんの手を決める
            Player1.setHand(scanner.nextInt());// プレイヤーのじゃんけんの手を読み込む
			Referee1.setJudge(Player1.getHand(),Enemy1.getHand());
            System.out.print("your_hand: "+ Player1.getVisualHand()); // プレイヤーの手の見出しを出力する
            System.out.print(", enemy_hand: "+Enemy1.getVisualHand()); // CPUの手の見出しを出力する
           	System.out.print(", judge: "+Referee1.getVisualJudge()); // 勝敗の見出しを出力する
            System.out.println(); // 改行
        }
    }
}